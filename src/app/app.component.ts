import { Component, inject, OnInit } from '@angular/core';
import { initFlowbite } from 'flowbite';
import { ClerkService } from 'ngx-clerk';
import { environment } from '../environments/environment';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrl: './app.component.scss',
})
export class AppComponent implements OnInit {
  private _clerk = inject(ClerkService);

  ngOnInit(): void {
    initFlowbite();
    this._clerk.__init({
      publishableKey: environment.CLERK_PUBLISHABLE_KEY,
    });
  }
}
